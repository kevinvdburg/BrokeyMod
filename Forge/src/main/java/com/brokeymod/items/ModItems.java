package com.brokeymod.items;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by kvdb on 19/01/16.
 */
public final class ModItems {
    public static Item tutorialItem;


    public static void createItems() {
        GameRegistry.registerItem(tutorialItem =  new BasicItem("tutorial_item"), "tutorial_item");
    }
}
