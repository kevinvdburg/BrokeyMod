package com.brokeymod.crafting;

import com.brokeymod.blocks.ModBlocks;
import com.brokeymod.items.ModItems;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by kvdb on 20/01/16.
 */
public class ModCrafting {
    public static void initCrafting(){
        GameRegistry.addRecipe(new ItemStack(ModBlocks.tutorialBlock), new Object[] {"##", "##", '#', ModItems.tutorialItem});
    }

    public static void initSmelting(){
        GameRegistry.addSmelting(ModBlocks.tutorialBlock, new ItemStack(ModItems.tutorialItem, 8), 0.5f);
    }
}
