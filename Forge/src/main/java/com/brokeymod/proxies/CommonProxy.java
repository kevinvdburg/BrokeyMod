package com.brokeymod.proxies;

import com.brokeymod.blocks.ModBlocks;
import com.brokeymod.crafting.ModCrafting;
import com.brokeymod.items.ModItems;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import world.TutorialWorldGen;

/**
 * Created by kvdb on 19/01/16.
 */
public class CommonProxy {
    public void preInit(FMLPreInitializationEvent e) {
        ModItems.createItems();
        ModBlocks.createBlock();
    }

    public void init(FMLInitializationEvent e) {
        ModCrafting.initCrafting();
        ModCrafting.initSmelting();
        GameRegistry.registerWorldGenerator(new TutorialWorldGen(), 0);
    }

    public void postInit(FMLPostInitializationEvent e) {

    }

}
