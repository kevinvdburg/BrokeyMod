package com.brokeymod.client.render.blocks;

import com.brokeymod.blocks.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import com.brokeymod.main.Main;

/**
 * Created by kvdb on 20/01/16.
 */
public class BlockRenderRegister {
    public static void registerBlockRenderer() {
        reg(ModBlocks.tutorialBlock);
        reg(ModBlocks.tutorialOre);
        reg(ModBlocks.tutorialMultiOre);
        reg(ModBlocks.propertyBlock);
    }

    public static String modid = Main.MODID;

    public static void reg(Block block) {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(block), 0, new ModelResourceLocation(modid + ":" + block.getUnlocalizedName().substring(5), "inventory"));

    }
}
