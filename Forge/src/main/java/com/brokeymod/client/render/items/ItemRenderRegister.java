package com.brokeymod.client.render.items;

import com.brokeymod.main.Main;
import com.brokeymod.items.ModItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;

/**
 * Created by kvdb on 19/01/16.
 */
public final class ItemRenderRegister {
    public static String modid = Main.MODID;
    public static void registerItemRenderer() {
        reg(ModItems.tutorialItem);

    }

    public static void reg(Item item){
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(ModItems.tutorialItem,0, new ModelResourceLocation(modid + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    };
}
