package com.brokeymod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

import java.util.Random;

/**
 * Created by kvdb on 20/01/16.
 */


public class ModBlockOre extends Block {
    private Item drop;
    private int meta;
    private int least_quantity;
    private int most_quantity;

    protected ModBlockOre(String unlocalizedName, Material mat, Item drop, int meta, int least_quantity, int most_quantity) {
        super(mat);
        this.drop = drop;
        this.meta = meta;
        this.least_quantity = least_quantity;
        this.most_quantity = most_quantity;
        this.setHarvestLevel("pickaxe", 1);
        this.setHardness(10.0f);
        this.setResistance(15.0f);
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }

    protected ModBlockOre(String unlocalizedName, Material mat, Item drop, int least_quantity, int most_quantity) {
        this(unlocalizedName, mat, drop, 0, least_quantity, most_quantity);
    }

    protected ModBlockOre(String unlocalizedName, Material mat, Item drop) {
        this(unlocalizedName, mat, drop, 1, 1);
    }

    @Override
    public Item getItemDropped(IBlockState blockstate, Random random, int fortune) {
        return this.drop;
    }

    @Override
    public int damageDropped(IBlockState blockstate) {
        return this.meta;
    }

    @Override
    public int quantityDropped(IBlockState blockstate, int fortune, Random random) {
        if (this.least_quantity >= this.most_quantity)
            return this.least_quantity;
        return this.least_quantity + random.nextInt(this.most_quantity - this.least_quantity + fortune + 1);
    }

    @Override
    public void onBlockClicked(World world, BlockPos pos, EntityPlayer player) {
        System.out.println("Clicked Block -" + pos.toString());
        //this.setLightLevel(10);
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if(!world.isRemote)
        {
            ItemStack itemStack = playerIn.getCurrentEquippedItem();
            if(itemStack != null)
            {
                Item item = itemStack.getItem();
                System.out.println("clicked with " + item.getUnlocalizedName());
            }
            else {
                System.out.println("Name: " + playerIn.getName() + " - UUID: " + playerIn.getUniqueID());
                playerIn.setFire(1);

                //world.getPlayerEntityByName(playerIn.getName()).setPosition(pos.getX(), (pos.getY() + 200), pos.getZ());
            }
        }
        return true;
    }



}
