package com.brokeymod.blocks;

import net.minecraft.item.ItemStack;

/**
 * Created by kvdb on 20/01/16.
 */
public interface IMetaBlockName {
    String getSpecialName(ItemStack stack);
}
