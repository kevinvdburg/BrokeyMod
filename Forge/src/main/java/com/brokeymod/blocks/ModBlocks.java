package com.brokeymod.blocks;

import com.brokeymod.items.ModItems;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by kvdb on 20/01/16.
 */
public class ModBlocks {
    public static Block tutorialBlock;
    public static Block tutorialOre;
    public static Block tutorialMultiOre;
    public static Block propertyBlock;

    public static void createBlock(){
        GameRegistry.registerBlock(tutorialBlock = new BasicBlock("tutorial_block", Material.rock, 1 ,1).setLightLevel(1.0f), "tutorial_block");
        GameRegistry.registerBlock(tutorialOre = new ModBlockOre("tutorial_ore", Material.rock, ModItems.tutorialItem, 2, 4), "tutorial_ore");
        GameRegistry.registerBlock(tutorialMultiOre = new ModBlockMultiOre("tutorial_multi_ore", Material.rock), "tutorial_multi_ore");
        GameRegistry.registerBlock(propertyBlock = new BlockProperties("block_properties"), ItemBlockMeta.class, "block_properties");

    }
}
