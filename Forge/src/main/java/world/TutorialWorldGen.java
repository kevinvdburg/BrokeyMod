package world;

import com.brokeymod.blocks.ModBlocks;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

/**
 * Created by kvdb on 20/01/16.
 */
public class TutorialWorldGen implements IWorldGenerator {
    private WorldGenerator gen_tutorial_ore;
    private WorldGenerator gen_multi_ore;

    public TutorialWorldGen(){
        this.gen_tutorial_ore = new WorldGenMinable(ModBlocks.tutorialOre.getDefaultState(), 8);
        this.gen_multi_ore = new WorldGenSingleMinable(ModBlocks.tutorialMultiOre.getDefaultState());
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
        switch (world.provider.getDimensionId()){
            case 0: //Overworld
                this.runGenerator(this.gen_tutorial_ore, world, random, chunkX, chunkZ, 20, 0, 64);
                this.runGenerator(this.gen_multi_ore, world, random, chunkX, chunkZ, 10, 0, 16);
                break;
            case 1: //Nether

                break;

            case 2: //End

                break;
        }
    }



    private void runGenerator(WorldGenerator generator, World world, Random rand, int chunk_X, int chunk_Z, int chancesToSpawn, int minHeight, int maxHeight) {
        if (minHeight < 0 || maxHeight > 256 || minHeight > maxHeight)
            throw new IllegalArgumentException("Illegal Height Arguments for WorldGenerator");

        int heightDiff = maxHeight - minHeight + 1;
        for (int i = 0; i < chancesToSpawn; i ++) {
            int x = chunk_X * 16 + rand.nextInt(16);
            int y = minHeight + rand.nextInt(heightDiff);
            int z = chunk_Z * 16 + rand.nextInt(16);
            generator.generate(world, rand, new BlockPos(x, y, z));
        }
    }
}
